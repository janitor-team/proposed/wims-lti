# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the wims-lti package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: wims-lti\n"
"Report-Msgid-Bugs-To: wims-lti@packages.debian.org\n"
"POT-Creation-Date: 2021-04-19 14:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "hostname for the service:"
msgstr ""

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"When you use Apache2 + WSGI to deploy the service, it can be implemented as "
"a virtual host. In the example below, the service administration's webpage "
"should be at http://wims-lti.example.com/admin/"
msgstr ""

#. Type: string
#. Description
#: ../templates:1001
msgid "If unsure, you can keep the default value."
msgstr ""

#. Type: string
#. Description
#: ../templates:2001
msgid "email for django's service admin:"
msgstr ""

#. Type: string
#. Description
#: ../templates:2001
msgid "Choose a valid email string."
msgstr ""

#. Type: password
#. Description
#: ../templates:3001
msgid "password for django's service admin:"
msgstr ""

#. Type: password
#. Description
#: ../templates:3001
msgid "Choose a strong enough password."
msgstr ""

#. Type: string
#. Description
#: ../templates:4001
msgid "how long to wait for a response from WIMS adm/raw module:"
msgstr ""

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Choose an integer value (in seconds). The default value is generally fine. "
"When a server has many active classes, this time should be increased."
msgstr ""
